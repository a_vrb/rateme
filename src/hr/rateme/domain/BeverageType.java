package hr.rateme.domain;

public enum BeverageType {
	COFFEE_ESPRESSO, COFFEE_MILK;
}
