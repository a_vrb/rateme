package hr.rateme.domain;

import hr.rateme.utils.Util;

import java.util.Date;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

public class Rating implements Comparable<Rating>, Parcelable{
	private int id;
	private String device;
	private Date date;
	private Map<String, Float> rates;
	
	public Rating() {
		super();
	}

	public Rating(String device, Date date, Map<String, Float> rates) {
		super();
		this.device = device;
		this.date = date;
		this.rates = rates;
	}
	
	public Rating(int id, String device, Date date, Map<String, Float> rates) {
		super();
		this.id = id;
		this.device = device;
		this.date = date;
		this.rates = rates;
	}

	public Rating(Parcel source) {
		super();
		this.id = source.readInt();
		this.device = source.readString();
		this.date = (Date)source.readValue(Date.class.getClassLoader());
		this.rates = Util.readParcelableMap(source);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDevice() {
		return device;
	}

	public void setUser(String device) {
		this.device = device;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Map<String, Float> getRates() {
		return rates;
	}

	public void setRates(Map<String, Float> rates) {
		this.rates = rates;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((rates == null) ? 0 : rates.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rating other = (Rating) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (rates == null) {
			if (other.rates != null)
				return false;
		} else if (!rates.equals(other.rates))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Rating [rates=" + rates + "]";
	}

	@Override
	public int compareTo(Rating another) {
		return this.date.compareTo(another.getDate());
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		
		dest.writeInt(this.id);
		dest.writeString(this.device);
		dest.writeValue(this.date);
		Util.writeParcelableMap(dest, this.rates);
	}
	
	public static final Parcelable.Creator<Rating> CREATOR = new Parcelable.Creator<Rating>() {
		@Override
		public Rating createFromParcel(Parcel source) {
			return new Rating(source);
		}

		@Override
		public Rating[] newArray(int size) {
			return new Rating[size];
		}

	};
	
}
