package hr.rateme.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Promotion implements Parcelable{
	private int id;
	private String name;
	private String description;
	
	public Promotion() {
		super();
	}

	public Promotion(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}
	
	public Promotion(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	
	public Promotion(Parcel source){
		this.id = source.readInt();
		this.name = source.readString();
		this.description = source.readString();
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Promotion other = (Promotion) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Promotion [name=" + name + ", description=" + description + "]";
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.name);
		dest.writeString(this.description);
		
	}
	
	
	public static final Parcelable.Creator<Promotion> CREATOR = new Parcelable.Creator<Promotion>() {
		@Override
		public Promotion createFromParcel(Parcel source) {
			return new Promotion(source);
		}

		@Override
		public Promotion[] newArray(int size) {
			return new Promotion[size];
		}

	};
	
	
}
