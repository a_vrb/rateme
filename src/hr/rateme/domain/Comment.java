package hr.rateme.domain;

import java.util.Date;

import android.os.Parcel;
import android.os.Parcelable;


public class Comment implements Parcelable {

	private int id;
	private String device;
	private String text;
	private Date date;

	public Comment(){
		super();
	}
	
	public Comment(String device, String text, Date date) {
		super();
		this.device = device;
		this.text = text;
		this.date = date;
	}
	
	public Comment(int id, String device, String text, Date date) {
		super();
		this.id=id;
		this.device = device;
		this.text = text;
		this.date = date;
	}
	
	public Comment(Parcel source) {
		super();
		this.id = source.readInt();
		this.device = source.readString();
		this.text = source.readString();
		this.date = (Date)source.readValue(ClassLoader.getSystemClassLoader());
		
	}
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getDevice() {
		return device;
	}



	public void setDevice(String device) {
		this.device = device;
	}



	public String getText() {
		return text;
	}



	public void setText(String text) {
		this.text = text;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((device == null) ? 0 : device.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (device == null) {
			if (other.device != null)
				return false;
		} else if (!device.equals(other.device))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [device=" + device + ", text=" + text + ", date=" + date
				+ "]";
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.id);
		dest.writeString(this.device);
		dest.writeString(this.text);
		dest.writeValue(this.date);
		
	}

	public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
		@Override
		public Comment createFromParcel(Parcel source) {
			return new Comment(source);
		}

		@Override
		public Comment[] newArray(int size) {
			return new Comment[size];
		}

	};
	
	
	
}
