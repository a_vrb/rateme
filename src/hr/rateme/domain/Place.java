package hr.rateme.domain;

import hr.rateme.utils.Constants;
import hr.rateme.utils.Predicate;
import hr.rateme.utils.RatingPeriod;
import hr.rateme.utils.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

public class Place implements Parcelable {
	
	private int id;
	private String name;
	private String address;
	private String pictureUrl;
	private List<Rating> ratings;
	private List<Comment> comments;
	private List<Promotion> promotions;
	private Map<String, Float> beverages;
	private String working_hours_from;
	private String working_hours_to;
	private Boolean wifi;
	private String coffee_distributor;
	private Smoking smoking;
	
	public Place(int id, String name, String address) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
	}

	public Place(String name, String address) {
		super();
		this.name = name;
		this.address = address;
	}


	public Place(int id, String name, String address, List<Rating> ratings,
			List<Comment> comments, List<Promotion> promotions) {
		super();
		this.id = id;
		this.name = name;
		this.address = address;
		this.ratings = ratings;
		this.comments = comments;
		this.promotions = promotions;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public String getPictureUrl() {
		return pictureUrl;
	}

	public String getPictureUrlEncoded() {
		return pictureUrl.replace(" ", "%20");
	}
	
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public List<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(List<Rating> ratings) {
		this.ratings = ratings;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<Promotion> promotions) {
		this.promotions = promotions;
	}

	public Map<String, Float> getBeverages() {
		return beverages;
	}

	public void setBeverages(Map<String, Float> beverages) {
		this.beverages = beverages;
	}

	public String getWorking_hours_from() {
		return working_hours_from;
	}

	public void setWorking_hours_from(String working_hours_from) {
		this.working_hours_from = working_hours_from;
	}

	public String getWorking_hours_to() {
		return working_hours_to;
	}

	public void setWorking_hours_to(String working_hours_to) {
		this.working_hours_to = working_hours_to;
	}

	public Boolean getWifi() {
		return wifi;
	}

	public void setWifi(Boolean wifi) {
		this.wifi = wifi;
	}

	public String getCoffee_distributor() {
		return coffee_distributor;
	}

	public void setCoffee_distributor(String coffee_distributor) {
		this.coffee_distributor = coffee_distributor;
	}

	public Smoking getSmoking() {
		return smoking;
	}

	public void setSmoking(Smoking smoking) {
		this.smoking = smoking;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Place other = (Place) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Place [id=" + id + ", name=" + name + ", address=" + address
				+ "]";
	}

	public Place(Parcel source) {
		this.id = source.readInt();
		this.name = source.readString();
		this.address = source.readString();
		this.pictureUrl = source.readString();
		source.readList(this.ratings = new ArrayList<Rating>(), Rating.class.getClassLoader());
		source.readList(this.comments = new ArrayList<Comment>(), Comment.class.getClassLoader());
		source.readList(this.promotions = new ArrayList<Promotion>(), Promotion.class.getClassLoader());
		
		this.working_hours_from = source.readString();
		this.working_hours_to = source.readString();
		this.wifi = (Boolean) source.readValue(Boolean.class.getClassLoader());
		this.coffee_distributor = source.readString();
		this.smoking = Smoking.valueOf(source.readString());
		this.beverages = Util.readParcelableMap(source);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(getId());
		dest.writeString(getName());
		dest.writeString(getAddress());
		dest.writeString(getPictureUrl());
		dest.writeList(getRatings());
		dest.writeList(getComments());
		dest.writeList(getPromotions());
		
		dest.writeString(getWorking_hours_from());
		dest.writeString(getWorking_hours_to());
		dest.writeValue(getWifi());
		dest.writeString(getCoffee_distributor());
		if(getSmoking() == null){
			setSmoking(Smoking.NO_DATA);
		}
		dest.writeString(getSmoking().toString());

		Util.writeParcelableMap(dest, this.beverages);
	}

	public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
		@Override
		public Place createFromParcel(Parcel source) {
			return new Place(source);
		}

		@Override
		public Place[] newArray(int size) {
			return new Place[size];
		}

	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public Rating getRatesAverage(RatingPeriod period){
		Calendar cal = Calendar.getInstance();
		
		switch(period){

		case WEEK:
			cal.add(Calendar.DATE, -7);
			break;

		case MONTH:
			cal.add(Calendar.MONTH, -1);
			break;
			
		case QUARTER:
			cal.add(Calendar.MONTH, -3);
			break;
			
		case YEAR:
			cal.add(Calendar.YEAR, -1);
			break;	
		
		}
		
		Date d = cal.getTime();
		return getAverage(filterRates(d));
		
		
	}
	
	
	
	 private List<Rating> filterRates(final Date d){
		 return new ArrayList<Rating>(Util.filter(this.ratings, new Predicate<Rating>() {

				@Override
				public boolean apply(Rating type) {
					return type.getDate().compareTo(d) >= 0 ? true : false;
				}
			}));
	 }
	 
	public Rating getOverallAverage(){
		return getAverage(this.ratings);
	}
	 
	private Rating getAverage(List<Rating> rates) {
		float g = 0, p = 0, s = 0;

		for (Rating rate : rates) {
			g += rate.getRates().get(Constants.CATEGORY_COFFEE);
			p += rate.getRates().get(Constants.CATEGORY_PERSONNEL);
			s += rate.getRates().get(Constants.CATEGORY_INTERIOR);
		}

		Map<String, Float> rates_map = new HashMap<String, Float>();
		rates_map.put(Constants.CATEGORY_COFFEE, g /= rates.size());
		rates_map.put(Constants.CATEGORY_PERSONNEL, p /= rates.size());
		rates_map.put(Constants.CATEGORY_INTERIOR, s /= rates.size());

		Rating r = new Rating();
		r.setRates(rates_map);
		return r;
	}

	public Float getTotalAverage() {
		float sum = 0, size = 0;
		
		size = this.ratings.size()*3;
		for (Rating rate : this.ratings) {
			sum += rate.getRates().get(Constants.CATEGORY_COFFEE)
				+ rate.getRates().get(Constants.CATEGORY_PERSONNEL)
				+ rate.getRates().get(Constants.CATEGORY_INTERIOR);
		}
		
		return sum/size;
	}
	
	public boolean hasUserVotedInPastWeek(String device){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -7);
		
		for(Rating rate : filterRates(cal.getTime())){
			if (rate.getDevice().equals(device)){
				return true;
			}
		}
		
		return false;
	}

}
