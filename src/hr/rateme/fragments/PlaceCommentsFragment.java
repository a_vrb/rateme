package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.data.ApiManager;
import hr.rateme.data.CommentsAdapter;
import hr.rateme.domain.Comment;
import hr.rateme.domain.Place;
import hr.rateme.utils.Constants;
import hr.rateme.utils.Util;

import java.util.Calendar;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.DatePickerDialog;
import android.content.Context;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

public class PlaceCommentsFragment extends Fragment implements
DatePickerDialog.OnDateSetListener {
	View mRootView;
	Place mSelectedPlace;
	Comment mComment;
	ListView commentsListView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSelectedPlace = this.getArguments().getParcelable(Constants.SELECTED_PLACE);
		mComment = new Comment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.mRootView = inflater.inflate(R.layout.fragment_comments, container, false);
		
		if(this.getArguments().getBoolean("isLoggedIn",false)){
			((TextView)mRootView.findViewById(R.id.comment_desc)).setVisibility(View.GONE);
		ArrayAdapter<Comment> adapter = new CommentsAdapter(getActivity(), android.R.layout.simple_list_item_2, android.R.id.text1, mSelectedPlace.getComments());

		this.commentsListView = (ListView) mRootView.findViewById(R.id.list_of_comments);
		this.commentsListView.setAdapter(adapter);
		}
		Button b = (Button) mRootView.findViewById(R.id.button1);
		b.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(final View v) {
	            addComment(v);
	        }
	    });
		ImageButton btn_date = (ImageButton) mRootView.findViewById(R.id.btn_date_picker);
		btn_date.setOnClickListener(new OnClickListener() {
	        @Override
	        public void onClick(final View v) {
	            showDatePickerDialog(v);
	        }
	    });
		return mRootView;
	}
	
	final public void addComment(View v){
		EditText e = (EditText) mRootView.findViewById(R.id.editText1);
	
		this.mComment.setDevice(Util.getXDevice(getActivity()));
		this.mComment.setText(e.getText().toString());
		this.mComment.setDate(this.mComment.getDate() != null ? this.mComment.getDate() : Calendar.getInstance().getTime());
		
		ApiManager.getPlacesService().saveComment(Util.getXDevice(getActivity()),this.mSelectedPlace.getId(), this.mComment, new Callback<Object>() {

			@Override
			public void failure(RetrofitError e) {
				Log.e("RateMe API", e.getStackTrace().toString());
			}

			@Override
			public void success(Object arg0, Response arg1) {
				// TODO Auto-generated method stub
				
			}
		});
		generateComment();
		e.setText("");
	    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);

	}
	
	public void generateComment(){
		this.mSelectedPlace.getComments().add(mComment);
		ArrayAdapter<Comment> adapter = new CommentsAdapter(getActivity(), android.R.layout.simple_list_item_2, android.R.id.text1, mSelectedPlace.getComments());
		this.commentsListView.setAdapter(adapter);
			
	}
	public void showDatePickerDialog(View v) {
		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		// Create a new instance of DatePickerDialog and return it
		DatePickerDialog date_picker = new DatePickerDialog(getActivity(), this, year, month, day);
		date_picker.show();
		//newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
		Log.i("RateMe", "Date picker: "+dayOfMonth +"-"+monthOfYear+1+"-"+year);
		Calendar cal = Calendar.getInstance();
		cal.set(year, monthOfYear, dayOfMonth);
		this.mComment.setDate(cal.getTime());
	}
}
