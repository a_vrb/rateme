package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.activities.RegisterActivity;
import hr.rateme.utils.LoginUtil;
import hr.rateme.utils.Util;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginFragment  extends Fragment{
	
	View root;
	Context context;
	

	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.fragment_login,container, false);
		context = getActivity();
		
		((EditText)root.findViewById(R.id.edit_text_username)).setText(Util.getFromSharedPrefs(context,"saved_username",""));
		
		Button btn_login = (Button)root.findViewById(R.id.btn_login);
		
		btn_login.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				doLogin();
			}
		});
		

		Button btn_register = (Button)root.findViewById(R.id.btn_register);
		
		btn_register.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity().getApplicationContext(), RegisterActivity.class);
				startActivity(intent);
			}
		});
		
		return root;
	}
	
	
	public void doLogin(){
		String username = ((EditText)root.findViewById(R.id.edit_text_username)).getText().toString();
		String password = ((EditText)root.findViewById(R.id.edit_text_password)).getText().toString();
		LoginUtil.saveToSharedPrefs(context,"saved_username",username);
		if(username.equals("") || password.equals("")){
			Toast.makeText(getActivity(), R.string.msg_error_no_u_p ,Toast.LENGTH_SHORT).show();
			return;
		}
		
		Log.d("RateMe", "DoLogin: [username="+username+"]");
		LoginUtil.login(getActivity(), username, password);
	//	Toast.makeText(getActivity(), "u; "+username +", p: "+password, Toast.LENGTH_SHORT).show();
	}
	

}
