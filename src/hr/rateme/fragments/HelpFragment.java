package hr.rateme.fragments;

import hr.rateme.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HelpFragment extends Fragment {
	
	View root;
	Context context;
	

	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.fragment_help,container, false);
		context = getActivity();

		return root;
	}
	

}
