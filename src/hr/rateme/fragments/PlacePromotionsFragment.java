package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.data.PromotionsExpandableListAdapter;
import hr.rateme.domain.Place;
import hr.rateme.utils.Constants;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

public class PlacePromotionsFragment extends Fragment {

	View rootView;
	Place selected_place;
	ExpandableListView promotions_list;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		selected_place = this.getArguments().getParcelable(Constants.SELECTED_PLACE);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.rootView = inflater.inflate(R.layout.fragment_place_promotions, container, false);
		
		promotions_list = (ExpandableListView) this.rootView.findViewById(R.id.promotion_list);
		PromotionsExpandableListAdapter adapter = new PromotionsExpandableListAdapter(this.selected_place.getPromotions());
		adapter.setInflater((LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE),this.getActivity());
		promotions_list.setAdapter(adapter);
		setGroupIndicatorToRight();
		return rootView;
	}

		
	    private void setGroupIndicatorToRight() {
	        /* Get the screen width */
	        DisplayMetrics dm = new DisplayMetrics();
	        this.getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
	        int width = dm.widthPixels;
	 
	        promotions_list.setIndicatorBounds(width - getDipsFromPixel(35), width
	                - getDipsFromPixel(5));
	    }
	 
	    // Convert pixel to dip
	    public int getDipsFromPixel(float pixels) {
	        // Get the screen's density scale
	        final float scale = getResources().getDisplayMetrics().density;
	        // Convert the dps to pixels, based on density scale
	        return (int) (pixels * scale + 0.5f);
	    }
}
