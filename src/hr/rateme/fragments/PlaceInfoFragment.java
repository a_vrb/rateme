package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.domain.BeverageType;
import hr.rateme.domain.Place;
import hr.rateme.domain.Smoking;
import hr.rateme.utils.Constants;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PlaceInfoFragment extends Fragment {
	private Place selected_place;
	private View rootView;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		selected_place = this.getArguments().getParcelable(Constants.SELECTED_PLACE);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_place_info,
				container, false);
		TextView address = getTextView(R.id.value_info_address);
		TextView working_hours = getTextView(R.id.value_info_working_hours);
		TextView wifi = getTextView(R.id.value_info_wifi);
		TextView smoking = getTextView(R.id.value_info_smoking);
		TextView coffee = getTextView(R.id.value_info_coffee);
		TextView coffee_price = getTextView(R.id.value_info_coffee_price);
		
		address.setText(selected_place.getAddress());
		String to = selected_place.getWorking_hours_from() != null ? selected_place.getWorking_hours_from() : "";
		String from = selected_place.getWorking_hours_to() != null ? selected_place.getWorking_hours_to() : "";
		working_hours.setText( to +" - "+ from);
		wifi.setText(getWifiStringValue(selected_place.getWifi()));
		smoking.setText(mapSmokingString(selected_place.getSmoking()));
		coffee.setText(selected_place.getCoffee_distributor() != null ? selected_place.getCoffee_distributor() : "");
		String espresso = selected_place.getBeverages().get(BeverageType.COFFEE_ESPRESSO.toString()) != null ? selected_place.getBeverages().get(BeverageType.COFFEE_ESPRESSO.toString()).toString() : "" ;
		String milk = selected_place.getBeverages().get(BeverageType.COFFEE_MILK.toString()) != null ? selected_place.getBeverages().get(BeverageType.COFFEE_MILK.toString()).toString() : "";
		coffee_price.setText(espresso+"/"+milk+" kn");
		
		return rootView;
	}
	
	private TextView getTextView(int resId){
		return (TextView) rootView.findViewById(resId);
	}
	
	private String mapSmokingString(Smoking s){
		switch (s){
			case ALLOWED:
				return getString(R.string.smoking_allowed);
			case NOT_ALLOWED:
				return getString(R.string.smoking_not_allowed);
			case SEPARATED_AREAS:
				return getString(R.string.smoking_separated_areas);
			default: return "N/A";
		}
	}
	
	private String getWifiStringValue(Boolean wifi){
		if(wifi == null)return "N/A";
		
		return Boolean.TRUE.equals(wifi) ? getString(R.string.has_wifi) : getString(R.string.no_wifi);
	}
}
