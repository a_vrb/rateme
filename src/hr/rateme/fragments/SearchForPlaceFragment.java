package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.activities.PlaceOverviewActivity;
import hr.rateme.data.ApiManager;
import hr.rateme.data.PlacesAdapter;
import hr.rateme.domain.Place;
import hr.rateme.utils.Constants;
import hr.rateme.utils.Util;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

public class SearchForPlaceFragment extends Fragment {

	//ArrayAdapter adapter;
	PlacesAdapter adapter;
	ListView placesListView;
	Context context;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_search_for_place,
				container, false);
		placesListView = (ListView) rootView.findViewById(R.id.list_of_places);
		context = this.getActivity();
		ApiManager.getPlacesService().getPlaces(Util.getXDevice(context),placesListener());
		initSearch(rootView);
		return rootView;
	}
	
	public void initSearch(View view){
		final EditText searchInput = (EditText) view.findViewById(R.id.search_field); 
		searchInput.addTextChangedListener(new TextWatcher() {

		    @Override
		    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
		        adapter.getFilter().filter(cs);
		       
		    }

		    @Override
		    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

		    @Override
		    public void afterTextChanged(Editable arg0) {}
		});
	}
	
	public void populatePlaces(final List<Place> places){
		
			//	this.adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, listContent);
				adapter = new PlacesAdapter(context, android.R.layout.simple_list_item_2, android.R.id.text1, places);

			placesListView.setAdapter(adapter);
			placesListView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v,
						int position, long id) {
				onPlaceClicked(adapter.getItem(position));
				}

			});
	}
	
	private void onPlaceClicked(Place p){
		ApiManager.getPlacesService().getPlaceData(Util.getXDevice(context),p.getId(), placeListener());
	}
	
	
	public  Callback<List<Place>> placesListener(){ 
		final ProgressDialog d = Util.createProgressDialog(this.getActivity());
		d.show();
		return new Callback<List<Place>>() { 

		@Override
		public void failure(RetrofitError e) {
			d.dismiss();
			e.printStackTrace();
			
		}
		@Override
		public void success(List<Place> places, Response arg1) {
			d.dismiss();

			populatePlaces(places);
		
		}
	};
	}
	public  Callback<Place> placeListener(){ 
		final ProgressDialog d = Util.createProgressDialog(this.getActivity());
		d.show();
		return new Callback<Place>() { 

		@Override
		public void failure(RetrofitError e) {
			d.dismiss();
			e.printStackTrace();
			
		}
		@Override
		public void success(Place p, Response arg1) {
			d.dismiss();
			Bundle b = new Bundle();
			b.putParcelable(Constants.SELECTED_PLACE, p);
			
			Intent intent = new Intent(context, PlaceOverviewActivity.class);
			intent.putExtras(b);
			context.startActivity(intent);
		}
	};
	}
}
