package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.activities.PlaceOverviewActivity;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.utils.Constants;
import hr.rateme.utils.Util;
import hr.rateme.widgets.PlaceInfoWindowAdapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ShowOnMapFragment extends Fragment implements LocationListener{
	private static final int ZOOM = 15; 
	private static final int RADIUS = 500;
	private static final int GPS_REQUEST_CODE = 99;
	private static final int MIN_DISTANCE = 50;
	private static final int MIN_TIME = 1000 * 10;
    private SupportMapFragment supportMapFragment;
    private GoogleMap mMap;
    private Location mCurrentLocation;
    private LocationManager lm;
    private boolean gps_enabled;
    private boolean network_enabled;
    private List<GeoLocation> mLocations;
    private Map<String,Place> markerMapping = new HashMap<String, Place>();
    

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_show_on_map, container,
				false);
		  supportMapFragment = SupportMapFragment.newInstance();
		  getChildFragmentManager().beginTransaction().replace(R.id.map, supportMapFragment).commit();
		  setLocationManager();
		  initSearchLogic(rootView);
		return rootView;
	}
	
	 @Override
	    public void onResume(){
	    super.onResume();
	        if(mMap == null) {
	            mMap = supportMapFragment.getMap();
	            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	            mMap.setMyLocationEnabled(true);
	            mMap.getUiSettings().setMyLocationButtonEnabled(false);
	            mMap.setInfoWindowAdapter(new PlaceInfoWindowAdapter(getActivity()));
	            mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
					
					@Override
					public void onInfoWindowClick(Marker marker) {
						Bundle b = new Bundle();
						b.putParcelable(Constants.SELECTED_PLACE, markerMapping.get(marker.getId()));
						
						Intent intent = new Intent(getActivity(), PlaceOverviewActivity.class);
						intent.putExtras(b);
						startActivity(intent);
					}
				});
	            Criteria criteria = new Criteria();
				criteria.setAccuracy(Criteria.ACCURACY_FINE);
				
	            mCurrentLocation = lm.getLastKnownLocation(lm.getBestProvider(criteria, false));
				if(mCurrentLocation!=null){
			      populateData(new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude()));
				 }
	        }
	       
	    }
	 
	 

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onLocationChanged(Location location) {
		
		mCurrentLocation = location;
		LatLng latLng= new LatLng(location.getLatitude(),location.getLongitude());
	    Log.i("RateMe Locaton", "Location changed");
	    populateData(latLng);
	       
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == GPS_REQUEST_CODE){
			setLocationManager();
		}
		
	}

	private void setLocationManager() {
        if (lm == null)
            lm = (LocationManager) this.getActivity()
                    .getSystemService(Context.LOCATION_SERVICE);

        // exceptions will be thrown if provider is not permitted.
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        	Log.e("GPS", ex.toString());
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        	Log.e("Network GPS", ex.toString());
        }

        if (gps_enabled) {
        	requestGPSLocation();
            Toast.makeText(this.getActivity(), R.string.msg_refresh_gps, Toast.LENGTH_LONG).show();
        } else {
        	AlertDialog d = createGPSDialog();
        	d.show();
        }
    }
	
	private void populateData(LatLng location){
		 addMyLocationMarker(this.mMap,location);
	     ApiManager.getPlacesService().getPlacesByLocation(Util.getXDevice(getActivity()),location.latitude,location.longitude,RADIUS,placesLocationListener());
	     
	}
	private void addMyLocationMarker(GoogleMap mMap, LatLng latLng){
		
	    mMap.clear();
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, ZOOM));
		mMap.addMarker(new MarkerOptions().position(latLng).title("I see you! :)"));
	    mMap.addCircle(new CircleOptions().center(latLng).radius(RADIUS).strokeWidth(1).strokeColor(Color.TRANSPARENT).fillColor(0x110000ff));
			
	}
	
	//creates dialog for enabling GSP if it is not enabled
	private AlertDialog createGPSDialog(){
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			
			builder.setTitle(R.string.gps_dialog_title).setMessage(R.string.gps_dialog_message).setIcon(R.attr.icon)
	        .setPositiveButton(R.string.gps_dialog_positive_btn, new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int id) {
	            	askForGPS();
	               }
	        })
	        .setNegativeButton(R.string.gps_dialog_negative_btn, new DialogInterface.OnClickListener() {
	            @Override
				public void onClick(DialogInterface dialog, int id) {
	            	requestNetLocation();
	            	Toast.makeText(getActivity(), R.string.msg_location_search_by_net, Toast.LENGTH_LONG).show();
	            }
	        });
			return builder.create();
		}
	//starts activity for GPS
	private void askForGPS(){
		this.startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), GPS_REQUEST_CODE);
	}
	//request location by GSP
	private void requestGPSLocation(){

			lm.removeUpdates(this);
			lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, MIN_DISTANCE, this);
		
	}

	//request location by network provider
	private void requestNetLocation(){
		lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
	}
	private void populateMap(){
		if(mLocations == null){
			return;
		}
		
		for (GeoLocation loc : mLocations) {
			LatLng latLng = new LatLng(loc.getLatitude(), loc.getLongitude());	
			Marker m = mMap.addMarker(new MarkerOptions().position(latLng).title(loc.getPlace().getName()).snippet(loc.getPlace().getTotalAverage().toString()));
			markerMapping.put(m.getId(),loc.getPlace());
		}

	}
	
	
	public  Callback<List<GeoLocation>> placesLocationListener(){ 
		final ProgressDialog d = Util.createProgressDialog(this.getActivity());
		d.show();
		return new Callback<List<GeoLocation>>() { 

		@Override
		public void failure(RetrofitError e) {
			d.dismiss();
			Log.e("RateMe API",e.getStackTrace().toString());
			
		}
		@Override
		public void success(List<GeoLocation> locations, Response arg1) {
			d.dismiss();
			mLocations = locations;
			populateMap();
		}
	};
	}
	
	private void initSearchLogic(View rootView){
		  

		final AutoCompleteTextView searchField = (AutoCompleteTextView)rootView.findViewById(R.id.map_search_field);
 
		final ImageButton searchBtn = (ImageButton)rootView.findViewById(R.id.map_search_btn);
		
		searchBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				searchAddress(searchField.getText().toString());
				
			}
		});
		
	}
	
	private void searchAddress(String address){
		Geocoder g = new Geocoder(this.getActivity());
        try {
        	List<Address> addresses = g.getFromLocationName(address,5);
 
        	if(addresses.size() > 0)
                {
				  populateData(new LatLng(addresses.get(0).getLatitude(),addresses.get(0).getLongitude()));
				  
                 }

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        
        
	}
	
}
