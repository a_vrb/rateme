package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.Place;
import hr.rateme.domain.Rating;
import hr.rateme.utils.Constants;
import hr.rateme.utils.RatingPeriod;
import hr.rateme.utils.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

public class RatePlaceFragment extends Fragment {

	List<RatingBar> ratings = new ArrayList<RatingBar>();
	Place selected_place;
	Button ratingDoneBtn;
	CheckBox cbRating;
	Spinner mPeriodSpinner;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		selected_place = this.getArguments().getParcelable(Constants.SELECTED_PLACE);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_rate_place,
				container, false);
		

		this.mPeriodSpinner = (Spinner) rootView.findViewById(R.id.spinner_rating_period);
		initPeriodSpinner(rootView);
		populateRates(rootView, RatingPeriod.WEEK);
		initBttns(rootView);
		
		return rootView;
	}
	
	private void initBttns(final View rootView){
		cbRating = (CheckBox) rootView.findViewById(R.id.checkBox1);
		ratingDoneBtn = (Button) rootView.findViewById(R.id.button1);
		
		if(selected_place.hasUserVotedInPastWeek(Util.getXDevice(getActivity()))){
			cbRating.setEnabled(false);
			cbRating.setVisibility(View.GONE);
			ratingDoneBtn.setEnabled(false);
			ratingDoneBtn.setVisibility(View.GONE);
			return;
		}
		cbRating.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					clearRatingBars(rootView);
					setRatingsTouchability(false);
					ratingDoneBtn.setVisibility(View.VISIBLE);
					cbRating.setClickable(false);
				}
			}
		});

		ratingDoneBtn.setVisibility(View.GONE);
		ratingDoneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				saveRating();
				Toast.makeText(getActivity(), R.string.msg_thanks_for_rating,Toast.LENGTH_SHORT).show();
				setRatingsTouchability(true);
				LinearLayout ll = (LinearLayout) rootView.findViewById(R.id.rating_options_layout);
				ll.setVisibility(View.GONE);
			}
		});
	}

	private void setRatingsTouchability(boolean isIndicator) {
		for (RatingBar rating : ratings) {
			rating.setIsIndicator(isIndicator);
		}
	}
	
	private void saveRating(){
		Rating r = new Rating(Util.getXDevice(getActivity()), Calendar.getInstance().getTime(), new HashMap<String,Float>(){{
			put(Constants.CATEGORY_COFFEE,ratings.get(0).getRating());
			put(Constants.CATEGORY_PERSONNEL,ratings.get(1).getRating());
			put(Constants.CATEGORY_INTERIOR,ratings.get(2).getRating());
		}});
		selected_place.getRatings().add(r);
		ApiManager.getPlacesService().saveRating(Util.getXDevice(getActivity()),selected_place.getId(),r,new Callback<Object>() {

			@Override
			public void failure(RetrofitError e) {
				Log.e("RateMe API", e.getStackTrace().toString());
			}

			@Override
			public void success(Object ratingId, Response arg1) {
				
			}
		});
	}
	
	private void initPeriodSpinner(final View rootView){
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				getActivity(), R.array.rating_periods,
				android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.mPeriodSpinner.setAdapter(adapter);
		this.mPeriodSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				populateRates(rootView,Util.getPeriod(position));
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}


		});
	}
	
	private void populateRates(View rootView, RatingPeriod period){
		Rating rates = this.selected_place.getRatesAverage(period);
		RatingBar r = (RatingBar) rootView.findViewById(R.id.rating_bar_coffee);
		r.setRating(rates.getRates().get(Constants.CATEGORY_COFFEE));
		RatingBar r1 = (RatingBar) rootView.findViewById(R.id.rating_bar_interior);
		r1.setRating(rates.getRates().get(Constants.CATEGORY_PERSONNEL));
		RatingBar r2 = (RatingBar) rootView.findViewById(R.id.rating_bar_staff);
		r2.setRating(rates.getRates().get(Constants.CATEGORY_INTERIOR));
		ratings.add(r);
		ratings.add(r1);
		ratings.add(r2);
		
		Rating overallRates = this.selected_place.getOverallAverage();
		RatingBar r3 = (RatingBar) rootView.findViewById(R.id.rating_bar_coffee_total);
		r3.setRating(overallRates.getRates().get(Constants.CATEGORY_COFFEE));
		RatingBar r4 = (RatingBar) rootView.findViewById(R.id.rating_bar_interior_total);
		r4.setRating(overallRates.getRates().get(Constants.CATEGORY_PERSONNEL));
		RatingBar r5 = (RatingBar) rootView.findViewById(R.id.rating_bar_staff_total);
		r5.setRating(overallRates.getRates().get(Constants.CATEGORY_INTERIOR));

	}
	
	private void clearRatingBars(View rootView){
		RatingBar r = (RatingBar) rootView.findViewById(R.id.rating_bar_coffee);
		r.setRating(0);
		RatingBar r1 = (RatingBar) rootView.findViewById(R.id.rating_bar_interior);
		r1.setRating(0);
		RatingBar r2 = (RatingBar) rootView.findViewById(R.id.rating_bar_staff);
		r2.setRating(0);
	}
	
}
