package hr.rateme.fragments;

import hr.rateme.R;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.BeverageType;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Smoking;
import hr.rateme.utils.Constants;
import hr.rateme.utils.Util;

import java.io.File;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

public class PlaceAdministrationFragment extends Fragment {

	private static final int PICK_IMAGE = 1;
	private View mRootView;
	private Place mSelectedPlace;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mSelectedPlace = this.getArguments().getParcelable(Constants.SELECTED_PLACE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.mRootView = inflater.inflate(R.layout.fragment_place_administration,container, false);
		((RadioGroup) mRootView.findViewById(R.id.radioWifi)).check(Boolean.TRUE.equals(mSelectedPlace.getWifi()) ? R.id.radioWifiYes : R.id.radioWifiNo);
		((RadioGroup) mRootView.findViewById(R.id.radioSmoking)).check(getSmokingRadioButtonId(mSelectedPlace.getSmoking()));
		initBtnUploadPic();
		initBtnSavePromotion();
		initBtnSavePlaceData();
		return mRootView;
	}
	
	private void initBtnUploadPic(){
		  Button selectImageButton = (Button) mRootView.findViewById(R.id.btn_upload_profile_pic);
		  selectImageButton.setOnClickListener(new OnClickListener() {

		   @Override
		   public void onClick(View v) {
		    selectImageFromGallery();

		   }
		  });
	}
	private void initBtnSavePromotion(){

		Button btn_save_promotion = (Button) mRootView.findViewById(R.id.btn_save_promotion);
		btn_save_promotion.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText promotion_name = (EditText) mRootView.findViewById(R.id.textfield_promotion_name);
				EditText promotion_text = (EditText) mRootView.findViewById(R.id.textfield_promotion_text);
				final Promotion promotion = new Promotion(promotion_name.getText().toString(), promotion_text.getText().toString());
				ApiManager.getPlacesService().savePromotion(Util.getXAuthToken(getActivity()),mSelectedPlace.getId(), promotion, promotionCallback());
				mSelectedPlace.getPromotions().add(promotion);
				
				promotion_name.setText("");
				promotion_text.setText("");
			}
		});
		
	}
	
	private void initBtnSavePlaceData(){

		Button btn_save_place_data = (Button) mRootView.findViewById(R.id.btn_save_place_data);
		btn_save_place_data.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText address = (EditText) mRootView.findViewById(R.id.input_address_field);
				EditText coffee_type = (EditText) mRootView.findViewById(R.id.input_coffe_type_field);
				EditText working_hour_from = (EditText) mRootView.findViewById(R.id.input_working_hours_from_field);
				EditText working_hour_to = (EditText) mRootView.findViewById(R.id.input_working_hours_to_field);
				EditText espresso_price = (EditText) mRootView.findViewById(R.id.input_coffe_espresso_price_field);
				EditText coffee_milk_price = (EditText) mRootView.findViewById(R.id.input_coffe_milk_price_field);
				
				Smoking smoke = getSmokingFromRadioButtonsForm((RadioGroup) mRootView.findViewById(R.id.radioSmoking));
				mSelectedPlace.setSmoking(smoke != null ? smoke : mSelectedPlace.getSmoking());
				RadioGroup rg = (RadioGroup) mRootView.findViewById(R.id.radioSmoking);
				int radiobtn_id = rg.getCheckedRadioButtonId();
				View radioButton = rg.findViewById(radiobtn_id);
				int selected_wifi_button = rg.indexOfChild(radioButton);
				
				boolean wifi = selected_wifi_button == 0 ? true : false;
				mSelectedPlace.setWifi(wifi);
				mSelectedPlace.setAddress(!address.getText().toString().equals("") ? address.getText().toString() : mSelectedPlace.getAddress());
				mSelectedPlace.setCoffee_distributor(!coffee_type.getText().toString().equals("") ? coffee_type.getText().toString() : mSelectedPlace.getCoffee_distributor());
				mSelectedPlace.setWorking_hours_from(!working_hour_from.getText().toString().equals("") ? working_hour_from.getText().toString() : mSelectedPlace.getWorking_hours_from());
				mSelectedPlace.setWorking_hours_to(!working_hour_to.getText().toString().equals("") ? working_hour_to.getText().toString() : mSelectedPlace.getWorking_hours_to());
				
				if(!espresso_price.getText().toString().equals(""))
				mSelectedPlace.getBeverages().put(BeverageType.COFFEE_ESPRESSO.toString(), Float.valueOf(espresso_price.getText().toString()));
				if(!coffee_milk_price.getText().toString().equals(""))
				mSelectedPlace.getBeverages().put(BeverageType.COFFEE_MILK.toString(), Float.valueOf(coffee_milk_price.getText().toString()));
				
				final ProgressDialog d = Util.createProgressDialog(getActivity());
				d.show();
				ApiManager.getPlacesService().updatePlaceData(Util.getXAuthToken(getActivity()), mSelectedPlace.getId(),mSelectedPlace, new Callback<Object>() {
					
					@Override
					public void success(Object arg0, Response arg1) {
						d.dismiss();
						Toast.makeText(getActivity(), getString(R.string.msg_place_update_success), Toast.LENGTH_LONG).show();
						
					}
					
					@Override
					public void failure(RetrofitError arg0) {
						d.dismiss();
						Toast.makeText(getActivity(), getString(R.string.error_place_update_failed), Toast.LENGTH_LONG).show();
						
					}
				});
				
			}
		});
		
	}
	
	private Smoking getSmokingFromRadioButtonsForm(RadioGroup rg){
		int radiobtn_id = rg.getCheckedRadioButtonId();
		View radioButton = rg.findViewById(radiobtn_id);
		int selected_smoking = rg.indexOfChild(radioButton);
		switch (selected_smoking){
		case 0:
			return Smoking.ALLOWED;
		case 1:
			return Smoking.NOT_ALLOWED;
		case 2:
			return Smoking.SEPARATED_AREAS;
		default:
			return null;
		}
		
	}
	
	private int getSmokingRadioButtonId(Smoking smoke){
		switch (smoke){
		case ALLOWED:
			return R.id.radioAllowed;
		case NOT_ALLOWED:
			return R.id.radioDisallowed;
		case SEPARATED_AREAS:
			return R.id.radioSeparatedAreas;
		default:
			return -1;
		}
	}
	
	/**
	* Opens dialog picker, so the user can select image from the gallery. The
	* result is returned in the method <code>onActivityResult()</code>
	*/
	public void selectImageFromGallery() {
	 Intent intent = new Intent();
	 intent.setType("image/*");
	 intent.setAction(Intent.ACTION_GET_CONTENT);
	 startActivityForResult(Intent.createChooser(intent, "Select Picture"),PICK_IMAGE);
	}
	
	/**
	* Retrives the result returned from selecting image, by invoking the method
	* <code>selectImageFromGallery()</code>
	*/
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	 super.onActivityResult(requestCode, resultCode, data);

	 if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
	  Uri selectedImage = data.getData();
	  TypedFile fileToSend = Util.getImageTypedFile(getActivity(), selectedImage);
	  
	  ApiManager.getPlacesService().uploadPlacePicture(Util.getXAuthToken(getActivity()),mSelectedPlace.getId(), fileToSend, new Callback<Object>() {

		@Override
		public void failure(RetrofitError arg0) {
			Log.e("picture upload", arg0.getStackTrace().toString());
			
		}

		@Override
		public void success(Object arg0, Response arg1) {
			Log.d("picture upload", "succes");
			Toast.makeText(getActivity(), R.string.msg_picture_uploaded, Toast.LENGTH_LONG).show();
		}
	});
	//  ImageView imageView = (ImageView) mRootView.findViewById(R.id.gallery_image);
    //  imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
	//  decodeFile(picturePath);
	  
	 }
	}
	
	public  Callback<Object> promotionCallback(){ 
		final ProgressDialog d = Util.createProgressDialog(this.getActivity());
		d.show();
		
		return new Callback<Object>() {

		@Override
		public void failure(RetrofitError e) {
			Log.e("RateMe API", e.getStackTrace().toString());
			d.dismiss();
		}

		@Override
		public void success(Object arg0, Response arg1) {
			d.dismiss();		
			Toast.makeText(getActivity(), "Promocija uspje�no pohranjena", Toast.LENGTH_SHORT).show();
		}
	};
	}
}
