package hr.rateme.utils;

public enum RatingPeriod {
	WEEK, MONTH, QUARTER, YEAR;
}
