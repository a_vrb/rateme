package hr.rateme.utils;

public class Constants {
 public static String SELECTED_PLACE = "SELECTED_PLACE";
 
 public static String CATEGORY_COFFEE = "COFFEE";
 public static String CATEGORY_INTERIOR = "INTERIOR";
 public static String CATEGORY_PERSONNEL = "PERSONNEL";
}
