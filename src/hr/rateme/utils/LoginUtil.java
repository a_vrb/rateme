package hr.rateme.utils;

import hr.rateme.R;
import hr.rateme.activities.PlaceOverviewActivity;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.Place;
import hr.rateme.domain.User;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

public class LoginUtil {
	public static void login(final Context context, final String username, final String password){

		 final ProgressDialog d = Util.createProgressDialog(context);
			d.show();
			ApiManager.getPlacesService().loginUser(Util.getXDevice(context), username, password, new Callback<User>() {
				
				@Override
				public void success(User user, Response res) {
					d.dismiss();
					for(Header h : res.getHeaders()){
						if(h.getName() != null && h.getName().equals(context.getString(R.string.x_auth_token))){
							saveToSharedPrefs(context,context.getString(R.string.x_auth_token), h.getValue());
							
						//	Toast.makeText(getActivity(), getString(R.string.x_auth_token) + ": " + sharedPref.getString(getString(R.string.x_auth_token), "no token in prefs"), Toast.LENGTH_SHORT).show();
							 List<Place> mPlaces = new ArrayList<Place>();
							 CharSequence[] places_array;
							 
							if (user.getPlaces().size() == 1) {
								startPlaceOverview(context,user.getPlaces().get(0));
							} else {
								mPlaces = user.getPlaces();
								places_array = new CharSequence[mPlaces.size()];
								for (int i = 0; i < places_array.length; i++) {
									places_array[i] = mPlaces.get(i).getName();
							}
								AlertDialog dialog = createSelectPlaceDialog(context,places_array,mPlaces);
								dialog.show();
							}
						}
						
					}
					
				}
				
				@Override
				public void failure(RetrofitError arg0) {
					d.dismiss();
					Toast.makeText(context, R.string.msg_error_login_failed, Toast.LENGTH_SHORT).show();
					
				}
			});
		
	}
	
	
	private static void startPlaceOverview(Context context, Place place){
		Bundle b = new Bundle();
		b.putParcelable(Constants.SELECTED_PLACE, place);
		b.putBoolean("isLoggedIn", true);
		
		Intent intent = new Intent(context, PlaceOverviewActivity.class);
		intent.putExtras(b);
		context.startActivity(intent);
		}
	
	private static AlertDialog createSelectPlaceDialog(final Context context,final CharSequence[] places_array, final List<Place> mPlaces){
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Koji lokal �elite pregledati?")
		           .setItems(places_array, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						startPlaceOverview(context,mPlaces.get(which));
						
					}
				});

   return builder.create();

	}
	
	public static void saveToSharedPrefs(Context context,String key, String value){
		SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(key, value);
		editor.commit();
	
	}
}
