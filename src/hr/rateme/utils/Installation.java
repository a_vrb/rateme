package hr.rateme.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

public class Installation {
    private static String sID = null;
    private static final String INSTALLATION = "INSTALLATION";
 
    public synchronized static String id(Context context) {
        if (sID == null) {
            File installation = new File(context.getFilesDir(), INSTALLATION);
            try {
                if (!installation.exists()) writeInstallationFile(installation, context);
                sID = readInstallationFile(installation);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return sID;
    }

    private static String readInstallationFile(File installation) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[(int) raf.length()];
        raf.readFully(bytes);
        raf.close();
        return new String(bytes);
    }
 
    private static void writeInstallationFile(File installation,Context context) throws IOException {
        FileOutputStream fos = new FileOutputStream(installation);
        String id = Secure.getString(
                context.getContentResolver(), Secure.ANDROID_ID);
            // Use the Android ID unless it's broken, in which case
            // fallback on deviceId,
            // unless it's not available, then fallback on a random
            // number which we store to a prefs file
            try {
                if (!"9774d56d682e549c".equals(id)) {
                	id = UUID.nameUUIDFromBytes(id.getBytes("utf8")).toString();
                } else {
                    final String deviceId = (
                        (TelephonyManager) context
                        .getSystemService(Context.TELEPHONY_SERVICE))
                        .getDeviceId();
                    id = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")).toString() : UUID.randomUUID().toString();
                }
            } catch (UnsupportedEncodingException e) {
                fos.close();
                throw new RuntimeException(e);
            }
        fos.write(id.getBytes());
        fos.close();
    }
}