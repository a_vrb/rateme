package hr.rateme.utils;

public interface Predicate<T> {
	boolean apply(T type);
}
