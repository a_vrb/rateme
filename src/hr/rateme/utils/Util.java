package hr.rateme.utils;

import hr.rateme.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import retrofit.mime.TypedFile;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Parcel;
import android.provider.MediaStore;

public class Util {
	
	public static ProgressDialog createProgressDialog(Context context) {
		ProgressDialog diag;
		diag = new ProgressDialog(context);
		diag.setMessage("Loading data...");
		diag.setIndeterminate(false);
		diag.setCancelable(false);

		return diag;
	}
	
	public static <T> Collection<T> filter(Collection<T> col, Predicate<T> predicate) {
		  Collection<T> result = new ArrayList<T>();
		  for (T element: col) {
		    if (predicate.apply(element)) {
		      result.add(element);
		    }
		  }
		  return result;
		}
	public static RatingPeriod getPeriod(int i){
		switch(i){
	case 0:
		return RatingPeriod.WEEK;
	case 1:
		return RatingPeriod.MONTH;
	case 2:
		return RatingPeriod.QUARTER;
	case 3:
		return RatingPeriod.YEAR;
	default:
		return RatingPeriod.WEEK;
	}
	}
	
	public static boolean isOnline(Context context) {
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    return netInfo != null && netInfo.isConnectedOrConnecting();
	}
	
	public static String getXAuthToken(Context c){
		SharedPreferences sharedPref = c.getSharedPreferences(c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		return sharedPref.getString(c.getString(R.string.x_auth_token), "no token in prefs");
	}
	
	public static String getXDevice(Context c){
		SharedPreferences sharedPref = c.getSharedPreferences(c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		return sharedPref.getString(c.getString(R.string.x_device), "no token in prefs");
	}
	
	public static String getFromSharedPrefs(Context c, String key, String default_value){
		SharedPreferences sharedPref = c.getSharedPreferences(c.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		return sharedPref.getString(key, default_value);
	
	}
	
	// For writing to a Parcel
	public static void writeParcelableMap(
	        Parcel parcel, Map<String, Float > map)
	{
	    parcel.writeInt(map.size());
	    for(Map.Entry<String, Float> e : map.entrySet()){
	        parcel.writeString(e.getKey());
	        parcel.writeFloat(e.getValue());
	    }
	}

	// For reading from a Parcel
	public static Map<String, Float> readParcelableMap(
	        Parcel parcel)
	{
	    int size = parcel.readInt();
	    Map<String, Float> map = new HashMap<String, Float>(size);
	    for(int i = 0; i < size; i++){
	        map.put(parcel.readString(),(Float)parcel.readFloat());
	    }
	    return map;
	}
	
	public static TypedFile getImageTypedFile(Context c, Uri selectedImage){
		  String[] filePathColumn = { MediaStore.Images.Media.DATA };

		  Cursor cursor = c.getContentResolver().query(selectedImage,
		    filePathColumn, null, null, null);
		  cursor.moveToFirst();

		  int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		  String picturePath = cursor.getString(columnIndex);
		  cursor.close();
		  String mimeType = "image/jpg";
		  File file = new File(picturePath);
		  return new TypedFile(mimeType, file);
	}
}
