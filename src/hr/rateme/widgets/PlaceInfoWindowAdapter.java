package hr.rateme.widgets;

import hr.rateme.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;

public class PlaceInfoWindowAdapter implements InfoWindowAdapter {
	Context context;
	View view;

	public PlaceInfoWindowAdapter(Context context) {
		this.context = context;

	}

	@Override
	public View getInfoContents(Marker marker) {
		if (marker.isInfoWindowShown()) {
			marker.hideInfoWindow();
			return null;
		}
		if (marker.getSnippet() != null) {
			LayoutInflater inflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			this.view = inflater.inflate(R.layout.info_window_place, null);
			return generatePlaceInfoWindow(view, marker);
		}
		
		this.view = new TextView(context);
		((TextView)this.view).setText(marker.getTitle());
		return this.view;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		if (marker.isInfoWindowShown()) {
			marker.hideInfoWindow();
			return null;
		}

		return null;
	}

	private View generatePlaceInfoWindow(View v, Marker marker) {
		final TextView title = (TextView) view.findViewById(R.id.title);
		final RatingBar rating = (RatingBar) view
				.findViewById(R.id.info_window_rating_bar);

		title.setText(marker.getTitle());
		rating.setRating(Float.parseFloat(marker.getSnippet()));

		return view;
	}
}
