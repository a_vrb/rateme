/**
 * 
 */
package hr.rateme.data.interfaces;

import hr.rateme.domain.Comment;
import hr.rateme.domain.Place;
import hr.rateme.domain.Rating;

import java.util.List;

/**
 * @author Antonio
 *
 */
public interface DataService {

	
	public List<Place> getPlaces();
	public List<Comment> getComments(int placeID);
	public List<Rating> getRatings();
	
}
