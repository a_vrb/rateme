package hr.rateme.data;

import hr.rateme.domain.Comment;
import hr.rateme.domain.Device;
import hr.rateme.domain.GeoLocation;
import hr.rateme.domain.Place;
import hr.rateme.domain.Promotion;
import hr.rateme.domain.Rating;
import hr.rateme.domain.User;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/**
 * Rest api manager
 * @author Antonio
 *
 */
public class ApiManager {
	private static final Object o = new Object();
	private static PlacesService service;
	public static final String URL = "http://test-avrb.rhcloud.com/rateme/";

	/**
	 * Api methods service
	 * @author Antonio
	 *
	 */
	public static interface PlacesService {
		
		/*@PUT("/users.json")
		void newUser(@Body String id, Callback<String> cb);*/
		
		/**
		 * Api call to fetch list of places
		 * 
		 * @param cb callback with filled list of places
		 */
		@Headers("Content-Type:application/json")
		@GET("/places")
		void getPlaces(@Header("X-AUTH-DEVICE") String device_uuid,Callback<List<Place>> cb);
		
		@Headers("Content-Type:application/json")
		@GET("/places/location")
		void getPlacesByLocation(@Header("X-AUTH-DEVICE") String device_uuid,@Query("latitude") double latitude,@Query("longitude") double longitude, @Query("radius") double radius, Callback<List<GeoLocation>> cb);
		/**
		 * Api call to fetch selected place
		 * @param id place id
		 * @param cb callback with filled place object
		 */
		@Headers("Content-Type:application/json")
		@GET("/places/{id}")
		void getPlaceData(@Header("X-AUTH-DEVICE") String device_uuid,@Path("id")int id,Callback<Place>cb);

		/**
		 * Api call to updated place data
		 * 
		 * @param token
		 * @param id
		 * @param place
		 * @param cb
		 */
		@Headers("Content-Type:application/json")
		@PUT("/places/{id}")
		void updatePlaceData(@Header("X-AUTH-Token") String token,@Path("id")int id, @Body Place place, Callback<Object>cb);
		
		/**
		 * Api call to save new place
		 * @param device_uuid
		 * @param place
		 * @param cb
		 */
		@Headers("Content-Type:application/json")
		@POST("/places")
		void createNewPlace(@Header("X-AUTH-DEVICE") String device_uuid, @Body Place place, Callback<Place>cb);
		/**
		 * Api call to save comment for place
		 * 
		 * @param place_id ID of place for which comment is being saved 
		 * @param comment Comment object to save
		 * @param cb callback
		 */
		@Headers("Content-Type:application/json")
		@POST("/places/{id}/comment")
		void saveComment(@Header("X-AUTH-DEVICE") String device_uuid,@Path("id") int place_id, @Body Comment comment, Callback<Object> cb);
		
		/**
		 * Api call to save rates for place
		 * 
		 * @param place_id ID of place for which rates are being saved
		 * @param rating Rating object to save
		 * @param cb callback
		 */
		@Headers("Content-Type:application/json")
		@POST("/places/{id}/rating")
		void saveRating(@Header("X-AUTH-DEVICE") String device_uuid,@Path("id") int place_id, @Body Rating rating, Callback<Object> cb);
		
		/**
		 * Api call to save promotion for place
		 * 
		 * @param place_id ID of place for which promotion is being saved
		 * @param promotion Promotion object to save
		 * @param cb callback
		 */
		@Headers("Content-Type:application/json")
		@POST("/places/{id}/promotion")
		void savePromotion(@Header("X-AUTH-TOKEN") String token,@Path("id") int place_id, @Body Promotion promotion, Callback<Object> cb);
		
		@Multipart
		@POST("/places/{id}/picture")
		void uploadPlacePicture(@Header("X-AUTH-TOKEN") String token,@Path("id") int place_id, @Part("file") TypedFile photo, Callback<Object> cb);
		
		@Headers("Content-Type:application/json")
		@POST("/auth/login/user")
		void loginUser(@Header("X-AUTH-DEVICE") String device_uuid,@Header("X-Username") String username,@Header("X-Password") String password,Callback<User> cb);
	
		@Headers("Content-Type:application/json")
		@POST("/register/user")
		void registerUser(@Header("X-AUTH-DEVICE") String device_uuid,@Body User user, Callback<Object> cb);
		
		@Headers("Content-Type:application/json")
		@POST("/register/device")
		void registerDevice(@Body Device device, Callback<Object> cb);
		
		@Headers("Content-Type:application/json")
		@POST("/register/device/{uuid}")
		void getDevice(@Header("X-AUTH-DEVICE") String device_uuid,@Path("uuid") String uuid, Callback<Device> cb);
	
	}

	/**
	 * 
	 * @return API service for application
	 */
	public static PlacesService getPlacesService() {
		if (service == null) {
			synchronized (o) {
				if (service == null) {
					RestAdapter.Log log = new RestAdapter.Log() {
						public void log(String message) {
							Log.d("RateMeApi", message);
						}
					};
					Gson gson=  new GsonBuilder().setDateFormat("dd-MM-yyyy").create();
					RestAdapter restAdapter = new RestAdapter.Builder()
							.setLog(log).setLogLevel(RestAdapter.LogLevel.FULL)
							.setEndpoint(URL)
							.setConverter(new GsonConverter(gson))
							.build();
					service = restAdapter.create(PlacesService.class);
				}
			}
		}
		return service;
	}
}
