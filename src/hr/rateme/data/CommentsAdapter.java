package hr.rateme.data;

import hr.rateme.domain.Comment;
import hr.rateme.domain.Place;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CommentsAdapter extends ArrayAdapter<Comment> {


	final int INVALID_ID = -1;
	
	//Two data sources, the original data and filtered data
    private List<Comment> originalData;
  //  private List<Place> filteredData;
	
    /*
    public PlacesAdapter(Context context, int resource, List<Place> originalData) {
		super(context, resource, originalData);
		this.originalData = originalData;
		this.filteredData = originalData;
		
	}*/
    public CommentsAdapter(Context context, int resource,int resource_id, List<Comment> originalData) {
		super(context, resource, resource_id, originalData);
		this.originalData = originalData;
		//this.filteredData = originalData;
		
	}
	
    
	  @Override
	public int getCount() {
		return originalData.size();
	}

	  
		@Override
		public Comment getItem(int position) {
			return originalData.get(position);
		}
		
		
		
	    @Override
		public long getItemId(int position) {
	
			return originalData.get(position).getId();
		}


		@Override
	    public boolean hasStableIds() {
	        return true;
	    }
	
	    @Override
		public View getView(int position, View convertView, ViewGroup parent) {
	    	
	    	View v = super.getView(position, convertView, parent);
	    	TextView text1 = (TextView) v.findViewById(android.R.id.text1);
            TextView text2 = (TextView) v.findViewById(android.R.id.text2);

            text1.setTextSize(20);
            text1.setText(getItem(position).getText());
            text2.setText( new SimpleDateFormat("dd.MM.yyyy").format(getItem(position).getDate()));
            
			return v;
		}

		
	    @Override
	    public boolean isEnabled(int position) {
	        return false;
	    }
	    
}
