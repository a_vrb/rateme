package hr.rateme.data;

import hr.rateme.R;
import hr.rateme.domain.Promotion;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class PromotionsExpandableListAdapter extends BaseExpandableListAdapter {

	private Activity activity;
	private LayoutInflater inflater;
	private List<Promotion> promotions;

	public PromotionsExpandableListAdapter(List<Promotion> promotions) {
		this.promotions = promotions;
	}

	public void setInflater(LayoutInflater inflater, Activity activity) {
		this.inflater = inflater;
		this.activity = activity;
	}

	@Override
	public int getGroupCount() {
		return this.promotions.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return this.promotions.get(groupPosition).getName();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return this.promotions.get(groupPosition).getDescription();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return this.promotions.get(groupPosition).getId();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return this.promotions.get(groupPosition).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.group_item, null);
		}
		TextView promotion_desc = (TextView) convertView.findViewById(R.id.promotion_name);
		promotion_desc.setText(this.promotions.get(groupPosition).getName());
		return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_item, null);
		}
		TextView promotion_desc = (TextView) convertView.findViewById(R.id.promotion_description);
		promotion_desc.setText(this.promotions.get(groupPosition).getDescription());
		return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}


}
