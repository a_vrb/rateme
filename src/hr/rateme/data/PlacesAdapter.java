package hr.rateme.data;

import hr.rateme.domain.Place;
import hr.rateme.domain.Place;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

public class PlacesAdapter extends ArrayAdapter<Place> {


	final int INVALID_ID = -1;
	
	//Two data sources, the original data and filtered data
    private List<Place> originalData;
    private List<Place> filteredData;
	
    /*
    public PlacesAdapter(Context context, int resource, List<Place> originalData) {
		super(context, resource, originalData);
		this.originalData = originalData;
		this.filteredData = originalData;
		
	}*/
    public PlacesAdapter(Context context, int resource,int resource_id, List<Place> originalData) {
		super(context, resource, resource_id, originalData);
		this.originalData = originalData;
		this.filteredData = originalData;
		
	}
	
    
	  @Override
	public int getCount() {
		return filteredData.size();
	}

	  
		@Override
		public Place getItem(int position) {
			return filteredData.get(position);
		}
		
		
		
	    @Override
		public long getItemId(int position) {
	
			return filteredData.get(position).getId();
		}


		@Override
	    public boolean hasStableIds() {
	        return true;
	    }
	
	    @Override
		public View getView(int position, View convertView, ViewGroup parent) {
	    	View v = super.getView(position, convertView, parent);
	    	TextView text1 = (TextView) v.findViewById(android.R.id.text1);
            TextView text2 = (TextView) v.findViewById(android.R.id.text2);
            text1.setText(getItem(position).getName());
            text2.setText(getItem(position).getAddress());

			return v;
		}

		@Override
		public Filter getFilter() {

			return new Filter() {
			
				@Override
				protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
					filteredData = (ArrayList<Place>)filterResults.values;
	                notifyDataSetChanged();
				}
				
				@Override
				protected FilterResults performFiltering(CharSequence charSequence) {
					FilterResults results = new FilterResults();

	                //If there's nothing to filter on, return the original data for your list
	                if(charSequence == null || charSequence.length() == 0)
	                {
	                    results.values = originalData;
	                    results.count = originalData.size();
	                }
	                else
	                {
	                    List<Place> filterResultsData = new ArrayList<Place>();

	                    for(Place data : originalData)
	                    {
	                        //Filter through originalData and compare each item to charSequence.
	                        if(data.getName().toUpperCase().contains(charSequence.toString().toUpperCase()) 
	                        		|| data.getAddress().toUpperCase().contains(charSequence.toString().toUpperCase()))
	                        {
	                            filterResultsData.add(data);
	                        }
	                    }            

	                    results.values = filterResultsData;
	                    results.count = filterResultsData.size();
	                }

	                return results;
				}
			};
		}
	    
	    
}
