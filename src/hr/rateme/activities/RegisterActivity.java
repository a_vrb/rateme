package hr.rateme.activities;

import hr.rateme.R;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.Device;
import hr.rateme.domain.Place;
import hr.rateme.domain.User;
import hr.rateme.utils.LoginUtil;
import hr.rateme.utils.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends FragmentActivity {
	private List<Integer> mSelectedItems = new ArrayList<Integer>();
	List<Place> mPlaces = new ArrayList<Place>();
	List<Place> selected_places = new ArrayList<Place>();
	private CharSequence[] places_array;
	private Context context;
	private String username, password;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);
		context = this;
		ApiManager.getPlacesService().getPlaces(Util.getXDevice(this),placesListener());
		Button btn_select_place = (Button)findViewById(R.id.btn_select_place);
		setup_btn_select_place(btn_select_place);
		
		Button btn_sign_up = (Button)findViewById(R.id.btn_sign_up);
		//setup_btn_select_place(btn_select_place);
		btn_sign_up.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				username = ((EditText)findViewById(R.id.edit_text_user)).getText().toString();
				password = ((EditText)findViewById(R.id.edit_text_pass)).getText().toString();
				String firstname = ((EditText)findViewById(R.id.edit_text_firstname)).getText().toString();
				String lastname = ((EditText)findViewById(R.id.edit_text_lastname)).getText().toString();
				
				for(int i : mSelectedItems){
					selected_places.add(mPlaces.get(i));
				}
				User u = new User(username,password,firstname,lastname);
				u.setPlaces(selected_places);
				u.setDevices(new ArrayList<Device>(){{add(new Device(Util.getXDevice(getApplicationContext())));}});
				if(username.equals("") || password.equals("") || selected_places.isEmpty()){
					Toast.makeText(context, getString(R.string.invalid_user_data), Toast.LENGTH_LONG).show();
					return;
				}
				doRegister(u);
				
			}
		});
		
	}
	

	private void setup_btn_select_place(Button btn_select_place){
		btn_select_place.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog ad = createSelectPlaceDialog();
				ad.show();
				
			}
		});
	}
	
	private AlertDialog createSelectPlaceDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Odaberite va�e lokale:")
		           .setMultiChoiceItems(places_array, null,
                      new DialogInterface.OnMultiChoiceClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int which,
                       boolean isChecked) {
                   if (isChecked) {
                       // If the user checked the item, add it to the selected items
                       mSelectedItems.add(which);
                   } else if (mSelectedItems.contains(which)) {
                       // Else, if the item is already in the array, remove it 
                       mSelectedItems.remove(Integer.valueOf(which));
                   }
               }
           })
    // Set the action buttons
           .setPositiveButton(android.R.string.ok, null)
           .setNegativeButton(R.string.lbl_my_place_is_missing, new DialogInterface.OnClickListener() {
               @Override
               public void onClick(DialogInterface dialog, int id) {
            	       Dialog place_dialog = createPlaceDialog(); 
            	       place_dialog.show();
               }
           });

    return builder.create();

	}
	
	
	public  Callback<List<Place>> placesListener(){ 
		final ProgressDialog d = Util.createProgressDialog(this);
		d.show();
		return new Callback<List<Place>>() { 

		@Override
		public void failure(RetrofitError e) {
			d.dismiss();
			e.printStackTrace();
			
		}
		@Override
		public void success(List<Place> places, Response arg1) {
			d.dismiss();
			mPlaces = places;
			places_array = new CharSequence [places.size()];
			for(int i = 0; i< places_array.length; i++){
				places_array[i] = places.get(i).getName() +", "+places.get(i).getAddress();
			}
	
		}
	};
	}
	
	private void doRegister(User user){
		ApiManager.getPlacesService().registerUser(Util.getXDevice(this), user, new Callback<Object>() {
			
			@Override
			public void success(Object object, Response res) {
				Log.d("RateMe", "Registering user: [username="+username+"]");
				LoginUtil.login(context, username, password);
			}
			
			@Override
			public void failure(RetrofitError e) {
				Toast.makeText(getApplicationContext(), R.string.msg_registration_failed, Toast.LENGTH_LONG).show();
				Log.d("RateMe","Error: registration failed for user:" + username + "!");
			}
		});
	}
	
	private Dialog createPlaceDialog(){
		final View view = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_register_place, null);

	   	
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.register_place)
        	.setView(view)
            .setPositiveButton(R.string.lbl_save, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String pName = ((EditText)view.findViewById(R.id.place_name_field)).getText().toString();
					String pAddress = ((EditText)view.findViewById(R.id.place_address_field)).getText().toString();
					
					Place p = new Place(pName, pAddress);
					
				   	final ProgressDialog pd = Util.createProgressDialog(context);
				   	pd.show();
					ApiManager.getPlacesService().createNewPlace(Util.getXDevice(context), p, new Callback<Place>() {
						
						@Override
						public void success(Place place, Response arg1) {
							pd.dismiss();
							selected_places.add(place);
							Toast.makeText(context, getString(R.string.save_place_success), Toast.LENGTH_LONG).show();
							
						}
						
						@Override
						public void failure(RetrofitError arg0) {
							pd.dismiss();
							Toast.makeText(context, getString(R.string.save_place_error), Toast.LENGTH_LONG).show();
							
						}
					});
				}
			})
            .setNegativeButton(android.R.string.cancel, null);
       return builder.create();
	}
}
