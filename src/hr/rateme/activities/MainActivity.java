package hr.rateme.activities;

import hr.rateme.R;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.Device;
import hr.rateme.domain.Place;
import hr.rateme.utils.Constants;
import hr.rateme.utils.Installation;
import hr.rateme.utils.Util;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		if(!Util.isOnline(this)){
			AlertDialog dialog = createNetworkDialog();
			dialog.show();
		}
		final ProgressDialog d = Util.createProgressDialog(this);

		String deviceId = Installation.id(this);
		
		if(isFirstLaunch()){
			d.show();
		ApiManager.getPlacesService().registerDevice(new Device(deviceId), new Callback<Object>() {
			
			@Override
			public void success(Object arg0, Response arg1) {
				d.dismiss();
				//Toast.makeText(c,"sucessful registration", Toast.LENGTH_SHORT).show();
				Log.d("RateMe", "Successfull registration!");
				
			}
			
			@Override
			public void failure(RetrofitError e) {
				d.dismiss();
				//Toast.makeText(c, "Error! " + e.getMessage(), Toast.LENGTH_SHORT).show();
				Log.d("RateMe", "Error! Already registered!");
			}
		});
		}
		
		SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString(getString(R.string.x_device),deviceId);
		editor.commit();
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
//		int id = item.getItemId();
//		if (id == R.id.action_settings) {
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}

	public void open(View v) {
		Intent intent = new Intent(getApplicationContext(), DisplaySelectionActivity.class);
		intent.putExtra("selected_option",Integer.valueOf((String)v.getTag()));
		startActivity(intent);
	}
	
	public  Callback<Place> deviceListener(){ 
		final ProgressDialog d = Util.createProgressDialog(this);
		d.show();
		return new Callback<Place>() { 

		@Override
		public void failure(RetrofitError e) {
			d.dismiss();
			e.printStackTrace();
			
		}
		@Override
		public void success(Place p, Response arg1) {
			d.dismiss();
			Bundle b = new Bundle();
			b.putParcelable(Constants.SELECTED_PLACE, p);

		}
	};
	}

	private AlertDialog createNetworkDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		builder.setTitle(R.string.start_internet).setMessage(R.string.no_internet_connection)
        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int id) {
               }
        });
		return builder.create();
	}
	
	private boolean isFirstLaunch(){
		final String PREFS_NAME = getString(R.string.preference_file_key);

		SharedPreferences settings = this.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

		if (settings.getBoolean("my_first_time", true)) {
		    //the app is being launched for first time, do something        

		    // record the fact that the app has been started at least once
		    settings.edit().putBoolean("my_first_time", false).commit(); 

		             // first time task
		    return true;
		}
		
		return false;
	}
}
