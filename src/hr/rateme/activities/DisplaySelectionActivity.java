package hr.rateme.activities;

import hr.rateme.R;
import hr.rateme.fragments.HelpFragment;
import hr.rateme.fragments.LoginFragment;
import hr.rateme.fragments.SearchForPlaceFragment;
import hr.rateme.fragments.ShowOnMapFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class DisplaySelectionActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection_display);
		
		int fragid = getIntent().getIntExtra("selected_option",0);
		if (savedInstanceState == null) {
			if (fragid == getResources().getInteger(R.integer.SEARCH_FOR_PLACE)) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.main_selection_container,new SearchForPlaceFragment()).commit();
			} else if (fragid == getResources().getInteger(R.integer.SHOW_ON_MAP)) {
				getSupportFragmentManager()
						.beginTransaction()
						.replace(R.id.main_selection_container,new ShowOnMapFragment()).commit();
			}else if (fragid == getResources().getInteger(R.integer.USER_LOGIN)) {
				getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.main_selection_container,new LoginFragment()).commit();
			}else if (fragid == getResources().getInteger(R.integer.HELP)) {
			getSupportFragmentManager()
			.beginTransaction()
			.replace(R.id.main_selection_container,new HelpFragment()).commit();
		}
		}
	}
}
