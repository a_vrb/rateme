package hr.rateme.activities;

import hr.rateme.R;
import hr.rateme.data.ApiManager;
import hr.rateme.domain.Place;
import hr.rateme.fragments.PlaceAdministrationFragment;
import hr.rateme.fragments.PlaceCommentsFragment;
import hr.rateme.fragments.PlaceInfoFragment;
import hr.rateme.fragments.PlacePromotionsFragment;
import hr.rateme.fragments.RatePlaceFragment;
import hr.rateme.utils.Constants;
import hr.rateme.utils.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.squareup.picasso.Picasso;

public class PlaceOverviewActivity extends FragmentActivity {

	private static final int PICK_IMAGE = 1;
	private ViewPager viewPager;
	private CustomPagerAdapter myPagerAdapter;
	private Place selected_place;
	private Boolean isLoggedIn;
	private List<Fragment> frags = new ArrayList<Fragment>();
	private Context context;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		selected_place = getIntent().getExtras().getParcelable(Constants.SELECTED_PLACE.toString());
		isLoggedIn = getIntent().getExtras().getBoolean("isLoggedIn", false);
		
		setContentView(R.layout.activity_place_overview);
		ImageView profilePic = (ImageView) findViewById(R.id.place_image);
		if(selected_place.getPictureUrl() != null){
		Picasso.with(context).load(selected_place.getPictureUrlEncoded()).resize(100,0).into(profilePic);
		}
		initData();
		viewPager = (ViewPager) findViewById(R.id.place_overview_pager);
		myPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), context);
		viewPager.setAdapter(myPagerAdapter);
		PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		tabs.setViewPager(viewPager);
		//ApiManager.getPlacesService().getPlaceData(selected_place.getId(), placeListener());
		ImageButton selectImageButton = (ImageButton) findViewById(R.id.imageButtonPicture);
		if(!isLoggedIn){
			selectImageButton.setVisibility(View.GONE);
		}else{
		  selectImageButton.setOnClickListener(new OnClickListener() {

		   @Override
		   public void onClick(View v) {
			   Intent intent = new Intent();
				 intent.setType("image/*");
				 intent.setAction(Intent.ACTION_GET_CONTENT);
				 startActivityForResult(Intent.createChooser(intent, "Select Picture"),PICK_IMAGE);

		   }
		  });
		}
	}

	private void initData(){

		TextView place_name = (TextView) findViewById(R.id.place_name);
		place_name.setText(this.selected_place.getName());
		((RatingBar)findViewById(R.id.ratingBarPlaceTotal)).setRating(selected_place.getTotalAverage());
		TextView place_address = (TextView) findViewById(R.id.place_address);
		place_address.setText(this.selected_place.getAddress());
	//	Toast.makeText(this, selected_place.toString(),Toast.LENGTH_LONG).show();

	}
	class CustomPagerAdapter extends FragmentPagerAdapter {

		Context mContext;
		List<String> mOptions = new ArrayList<String>();
		
		public CustomPagerAdapter(FragmentManager fm, Context context) {
			super(fm);
			mContext = context;
			String[] listContent = getResources().getStringArray(R.array.place_overview_options);
			for(String item : listContent){
				if(!isLoggedIn && item.equalsIgnoreCase(getString(R.string.admin_option_qname))){
					continue;
				}
				mOptions.add(item);
			}
		}

		@Override
		public Fragment getItem(int position) {
			// Create fragment object
			Fragment fragment;

			if (position == getResources().getInteger(
					R.integer.PLACE_OVERVIEW_INFO)) {
				fragment = new PlaceInfoFragment();
			}else if (position == getResources().getInteger(
					R.integer.PLACE_OVERVIEW_RATINGS)) {
				fragment = new RatePlaceFragment();
			} else if (position == getResources().getInteger(
					R.integer.PLACE_OVERVIEW_COMMENTS)) {
				fragment = new PlaceCommentsFragment();
			} else if (position == getResources().getInteger(
					R.integer.PLACE_OVERVIEW_PROMOTIONS)) {
				fragment = new PlacePromotionsFragment();

			}else if (position == getResources().getInteger(R.integer.PLACE_OVERVIEW_ADMINISTRATION)) {
				fragment = new PlaceAdministrationFragment();

			} else {
				fragment = null;
				//Toast.makeText(getApplicationContext(), R.string.error,Toast.LENGTH_SHORT).show();
				Log.d("RateMe", getString(R.string.error));
			}
			
			  // Attach some data to the fragment 
			// that we'll use to populate  our fragment layouts 
			  Bundle args = new Bundle();
			  args.putParcelable(Constants.SELECTED_PLACE, selected_place);
			  args.putBoolean("isLoggedIn", isLoggedIn);
			  // Set the arguments on the fragment 
			  // that will be fetched in the 
			  // Fragment@onCreateView 
			  fragment.setArguments(args);
			  frags.add(fragment);
			return fragment;
		}

		@Override
		public int getCount() {
			return mOptions.size();
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return mOptions.get(position);
		}

	}
	public  Callback<Place> placeListener(){ 
		final ProgressDialog d = Util.createProgressDialog(this);
		d.show();
		return new Callback<Place>() { 

		@Override
		public void failure(RetrofitError e) {
			d.dismiss();
			e.printStackTrace();
			
		}
		@Override
		public void success(Place p, Response arg1) {
			d.dismiss();
			selected_place = p;
			request_done();
		}
	};
	}

	
	public void  request_done(){
		
		initData();
		viewPager = (ViewPager) findViewById(R.id.place_overview_pager);
		myPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager(), context);
		viewPager.setAdapter(myPagerAdapter);
		PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		tabs.setViewPager(viewPager);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	 super.onActivityResult(requestCode, resultCode, data);

	 if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
	  Uri selectedImage = data.getData();
	  TypedFile fileToSend = Util.getImageTypedFile(this, selectedImage);
	  final Context c = this;
	  ApiManager.getPlacesService().uploadPlacePicture(Util.getXAuthToken(this),selected_place.getId(), fileToSend, new Callback<Object>() {

		@Override
		public void failure(RetrofitError arg0) {
			Log.e("picture upload", arg0.getStackTrace().toString());
			
		}

		@Override
		public void success(Object arg0, Response arg1) {
			Log.d("picture upload", "succes");
			Toast.makeText(c, R.string.msg_picture_uploaded, Toast.LENGTH_LONG).show();
		}
	});

	  
	 }
	}
}
